package middleware

import (
	"gorm.io/gorm"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/cast"
	"gitlab.com/itbis/naura-api/internal/entity"
)

// GoMiddleware represent the data-struct for middleware
type GoMiddleware struct {
	// another stuff , may be needed by middleware
	JwtSecret  string
	JwtExpired int
	IsLoggedIn echo.MiddlewareFunc
	db     *gorm.DB
}

// CORS will handle the CORS middleware
func (m *GoMiddleware) CORS(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set("Access-Control-Allow-Origin", "*")
		c.Response().Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE")
		c.Response().Header().Set("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Token, Access-Control-Allow-Headers, Authorization, X-Requested-With")
		if c.Request().Method == "OPTIONS" {
			//w.WriteHeader(http.StatusOK)
			return c.JSON(http.StatusAccepted, "")
		}

		return next(c)
	}
}

// CORS will handle the CORS middleware
func (m *GoMiddleware) CreateToken(name, email, partnerId string, rolesId, exp int) (string, error) {

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)
	tokenString := ""
	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["name"]      = name
	claims["username"]  = email
	claims["partnerId"] = partnerId
	claims["rolesId"]   = rolesId
	claims["exp"]       = time.Now().Add(time.Hour * time.Duration(exp)).Unix()
	// Generate encoded token and send it as response.
	tokenString, err    := token.SignedString([]byte(m.JwtSecret))
	return tokenString, err

}

func (m *GoMiddleware) IsAdmin(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		user    := c.Get("user").(*jwt.Token)
		claims  := user.Claims.(jwt.MapClaims)
		rolesId := cast.ToInt(claims["rolesId"])
		if rolesId != 1 {
			return echo.ErrUnauthorized
		}
		return next(c)
	}
}

/*func (m *GoMiddleware) IsBlackList(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		user    := c.Get("user").(*jwt.Token)
		claims  := user.Claims.(jwt.MapClaims)
		email   = cast.ToString(claims["username"])


		if user2.Blacklist == "disallowed" {
			return echo.ErrUnauthorized
		}
		return next(c)
	}
}*/


func (m *GoMiddleware) GetInfo(c echo.Context) entity.User {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	item := entity.User{}

	item.RolesId = cast.ToInt(claims["rolesId"])
	item.Email = cast.ToString(claims["username"])
	item.PartnerId = cast.ToString(claims["partnerId"])
	return item

}
func (m *GoMiddleware) SetHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		rolesId := cast.ToString(claims["rolesId"])
		username := cast.ToString(claims["username"])
		partnerId := cast.ToString(claims["partnerId"])
		c.Request().Header.Add("rolesId", rolesId)
		c.Request().Header.Set("username", username)
		c.Request().Header.Set("partnerId", partnerId)

		return next(c)
	}
}


// InitMiddleware initialize the middleware
func InitMiddleware(jwtSecret string, jwtExpired int) *GoMiddleware {
	isLoggedIn := middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(jwtSecret),
	})
	return &GoMiddleware{
		JwtSecret:  jwtSecret,
		JwtExpired: jwtExpired,
		IsLoggedIn: isLoggedIn,
	}
}
