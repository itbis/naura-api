package util

import (
	"crypto/tls"
	"time"

	//"encoding/json"
	"strings"
	//"sync"
	//"time"
	"github.com/spf13/cast"
	"gopkg.in/gomail.v2"
)

func SubmitEmail(uuid string, smptp, port, user, pwd, from string, to string, subject string, body string) string {

	toArray := strings.Split(to, ",")
	//ccArray := strings.Split(cc, ",")
	d := gomail.NewDialer(smptp, cast.ToInt(port), user, pwd)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	m := gomail.NewMessage()
	m.SetHeader("From", from)
	m.SetHeader("To", toArray...)
	m.SetHeader("Subject", subject)
	/*if len(ccArray) > 0 {
		m.SetAddressHeader("Cc", cc, cc)
	}
	*/
	m.SetBody("text/html", body)
	//m.Attach("/home/Alex/lolcat.jpg")
	resp := "OK"
	if err := d.DialAndSend(m); err != nil {
		//panic(err)
		resp = "NOK:" + err.Error()
	}

	//insert history
	/*ds := config.GetString("ds.config")
	db, _, err := database.Connect(ds)
	if err != nil {
		logger.Debug("error" + err.Error())
	}
	insertHistory := "insert into promo_schedule_history (`schedule_id`,`to`,`group_id`,`subject`,`resp`,`created_at`) value ('" + ID + "','" + to + "','" + groupID + "','" + subject + "','" + resp + "',NOW())"
	_, _, err = database.Execute(db, insertHistory)
	logger.Debug("insertHistory:", insertHistory)
	if err != nil {
		logger.Debug("error" + err.Error())
	}
	*/
	return resp

}

func FormatDate(date time.Time) time.Time {
	//mydate := cast.ToTime(date).Format("2006-01-02 15:04:05")
	//mydate := date.Format("02/01/2006 15:04:05")
	//return string(mydate)
	return date
}
