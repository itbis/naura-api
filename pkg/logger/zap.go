package logger

import (
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	rotateLogs "github.com/lestrrat-go/file-rotatelogs"
)

type Logger struct {
	Tdr    *zap.Logger
	Syslog *zap.Logger
}

func NewZapLogger(stdout bool, path string, maxAge time.Duration) (logger *zap.Logger) {
	if !stdout && maxAge <= 0 {
		panic("max age is zero")
	}

	var writer zapcore.WriteSyncer
	if stdout {
		writer = zapcore.AddSync(os.Stdout)
	} else {
		rotate, err := rotateLogs.New(
			path+".%Y%m%d",
			rotateLogs.WithLinkName(path),
			rotateLogs.WithMaxAge(maxAge*24*time.Hour),
			rotateLogs.WithRotationTime(time.Hour),
		)
		if err != nil {
			panic(err)
		}
		writer = zapcore.AddSync(rotate)
	}

	core := zapcore.NewCore(getEncoder(), writer, zapcore.DebugLevel)
	logger = zap.New(core)
	return
}

func getEncoder() zapcore.Encoder {
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "time",
		MessageKey:     "data",
		EncodeDuration: millisDurationEncoder,
		EncodeTime:     timeEncoder,
		LineEnding:     zapcore.DefaultLineEnding,
	}

	return zapcore.NewJSONEncoder(encoderConfig)
}

func timeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format("2006-01-02 15:04:05.999"))
}

func millisDurationEncoder(d time.Duration, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendInt64(d.Nanoseconds() / 1000000)
}
