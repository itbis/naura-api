#!/usr/bin/env bash
#./build.sh gitlab.com/{2}/{3}/{4}
version=$1
CURRENT=`pwd`
package_name=`basename "$CURRENT"`
echo $package_name




#platforms=("windows/amd64" "linux/amd64" "darwin/amd64")
platforms=("linux/amd64")

for platform in "${platforms[@]}"
do
    platform_split=(${platform//\// })
    GOOS=${platform_split[0]}
    GOARCH=${platform_split[1]}
    #output_name=$package_name'-'$GOOS'-'$GOARCH
    
    output_name=$package_name
    if [ "$version" != "" ]; then
        output_name=$package_name'-'$version 
    fi

    if [ $GOOS = "windows" ]; then
        output_name+='.exe'
    fi  

    env GOOS=$GOOS GOARCH=$GOARCH go build -o $output_name $package
    if [ $? -ne 0 ]; then
        echo 'An error has occurred! Aborting the script execution...'
        exit 1
    fi
    #additional
    echo 'Success build' $output_name 
    mv $output_name bin/
done

