package adhoclog

import (
	routing "github.com/go-ozzo/ozzo-routing/v2"
	"gitlab.com/itbis/naura-api/internal/errors"
	"gitlab.com/itbis/naura-api/pkg/log"
	"gitlab.com/itbis/naura-api/pkg/pagination"
)

// RegisterHandlers sets up the routing of the HTTP handlers.
func RegisterHandlers(r *routing.RouteGroup, service Service, authHandler routing.Handler, logger log.Logger) {
	res := resource{service, logger}

	r.Use(authHandler)

	r.Get("/adhoclog/<id>", res.get)
	r.Get("/adhoclog", res.query)

}

type resource struct {
	service Service
	logger  log.Logger
}

func (r resource) get(c *routing.Context) error {
	adhoclog, err := r.service.Get(c.Request.Context(), c.Param("id"))
	if err != nil {
		return err
	}

	return errors.Success(adhoclog)
}

func (r resource) query(c *routing.Context) error {
	ctx := c.Request.Context()
	count, err := r.service.Count(ctx)
	if err != nil {
		return err
	}
	pages := pagination.NewFromRequest(c.Request, count)
	adhoclog, err := r.service.Query(ctx, pages.Offset(), pages.Limit())
	if err != nil {
		return err
	}
	pages.Items = adhoclog
	return errors.Success(pages)
}
