package bidloan

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/log"
)

// Service encapsulates usecase logic for bidloans.
type Service interface {
	Get(ctx context.Context, id string) (Bidloan, error)
	GetByPrice(ctx context.Context, id string) ([]Bidloan, error)
	Query(ctx context.Context, offset, limit int) ([]Bidloan, error)
	Count(ctx context.Context) (int, error)
	Create(ctx context.Context, input CreateRequest) (Bidloan, error)
	Update(ctx context.Context, id string, input UpdateRequest) (Bidloan, error)
	Delete(ctx context.Context, id string) (Bidloan, error)
}

// Bidloan represents the data about an bidloan.
type Bidloan struct {
	entity.Bidloan
}

// CreateRequest represents an bidloan creation request.
type CreateRequest struct {
	LoanBusinessID    string `json:"loan_business_id"`
	LoanOfferID       string `json:"loan_offer_id"`
	LoanPrice         int    `json:"loan_price"`
	PaymentBusinessID string `json:"payment_business_id"`
	PaymentOfferID    string `json:"payment_offer_id"`
}

// Validate validates the CreateRequest fields.
func (m CreateRequest) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.LoanBusinessID, validation.Required, validation.Length(0, 128)),
	)
}

// UpdateRequest represents an bidloan update request.
type UpdateRequest struct {
	LoanBusinessID    string `json:"loan_business_id"`
	LoanOfferID       string `json:"loan_offer_id"`
	LoanPrice         int    `json:"loan_price"`
	PaymentBusinessID string `json:"payment_business_id"`
	PaymentOfferID    string `json:"payment_offer_id"`
}

// Validate validates the CreateRequest fields.
func (m UpdateRequest) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.LoanBusinessID, validation.Required, validation.Length(0, 128)),
	)
}

type service struct {
	repo   Repository
	logger log.Logger
}

// NewService creates a new bidloan service.
func NewService(repo Repository, logger log.Logger) Service {
	return service{repo, logger}
}

// Get returns the bidloan with the specified the bidloan ID.
func (s service) Get(ctx context.Context, id string) (Bidloan, error) {
	bidloan, err := s.repo.Get(ctx, id)
	if err != nil {
		return Bidloan{}, err
	}
	return Bidloan{bidloan}, nil
}

// Create creates a new bidloan.
func (s service) Create(ctx context.Context, req CreateRequest) (Bidloan, error) {
	if err := req.Validate(); err != nil {
		return Bidloan{}, err
	}

	//id := entity.GenerateID()
	//now := time.Now()
	err := s.repo.Create(ctx, entity.Bidloan{
		LoanBusinessID:    req.LoanBusinessID,
		LoanOfferID:       req.LoanOfferID,
		LoanPrice:         req.LoanPrice,
		PaymentBusinessID: req.PaymentBusinessID,
		PaymentOfferID:    req.PaymentOfferID,
	})
	if err != nil {
		return Bidloan{}, err
	}
	return s.Get(ctx, req.LoanBusinessID)
}

// Update updates the bidloan with the specified ID.
func (s service) Update(ctx context.Context, id string, req UpdateRequest) (Bidloan, error) {
	if err := req.Validate(); err != nil {
		return Bidloan{}, err
	}

	bidloan, err := s.Get(ctx, id)
	if err != nil {
		return bidloan, err
	}
	/*bidloan.LoanBusinessID = req.LoanBusinessID
	bidloan.LoanOfferID = req.LoanOfferID
	bidloan.LoanPrice = req.LoanPrice
	bidloan.PaymentBusinessID = req.PaymentBusinessID
	bidloan.PaymentOfferID = req.PaymentOfferID
	*/

	if err := s.repo.Update(ctx, bidloan.Bidloan); err != nil {
		return bidloan, err
	}
	return bidloan, nil
}

// Delete deletes the bidloan with the specified ID.
func (s service) Delete(ctx context.Context, id string) (Bidloan, error) {
	bidloan, err := s.Get(ctx, id)
	if err != nil {
		return Bidloan{}, err
	}
	if err = s.repo.Delete(ctx, id); err != nil {
		return Bidloan{}, err
	}
	return bidloan, nil
}

// Count returns the number of bidloans.
func (s service) Count(ctx context.Context) (int, error) {
	return s.repo.Count(ctx)
}

// Query returns the bidloans with the specified offset and limit.
func (s service) Query(ctx context.Context, offset, limit int) ([]Bidloan, error) {
	items, err := s.repo.Query(ctx, offset, limit)
	if err != nil {
		return nil, err
	}
	result := []Bidloan{}
	for _, item := range items {
		result = append(result, Bidloan{item})
	}
	return result, nil
}

// Query returns the bidloans with the specified offset and limit.
func (s service) GetByPrice(ctx context.Context, id string) ([]Bidloan, error) {
	items, err := s.repo.QueryByPrice(ctx, id)
	if err != nil {
		return nil, err
	}
	result := []Bidloan{}
	for _, item := range items {
		result = append(result, Bidloan{item})
	}
	return result, nil
}
