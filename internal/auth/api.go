package auth

import (
	"fmt"
	"github.com/labstack/echo/v4"
	mw "github.com/labstack/echo/v4/middleware"
	"gitlab.com/itbis/naura-api/internal/audit_trail"
	"gitlab.com/itbis/naura-api/internal/user"
	"gitlab.com/itbis/naura-api/middleware"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/shared"
)

// Handler  represent the httphandler
type handler struct {
	service      Service
	auditService audit_trail.Service
	userService  user.Service
	logger       logger.Logger
	middware     *middleware.GoMiddleware
}

var IsLoggedIn = mw.JWTWithConfig(mw.JWTConfig{
	SigningKey: []byte("Naura#2021"),
})

// RegisterHandler ...
func RegisterHandler(e *echo.Echo, service Service, auditService audit_trail.Service, userService user.Service, logger logger.Logger, middware *middleware.GoMiddleware) {
	h := &handler{
		service:      service,
		auditService: auditService,
		userService: userService,
		logger:       logger,
		middware:     middware,
	}
	v := e.Group("/naura/v1")
	v.POST("/login", h.Login)
	v.POST("/reset", h.Login)

	//v.Use(middware.IsLoggedInx)
	v.GET("/me", h.Me, middware.IsLoggedIn)

}

type Request struct {
}


// Get will fetch the item based on given params
func (h *handler) Login(c echo.Context) error {

	type RequestLogin struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	ctx := c.Request().Context()

	var req RequestLogin
	var resp shared.Response
	err := c.Bind(&req)
	if err != nil {
		resp = shared.StatusUnprocessableEntity("")
		h.auditService.Insert(ctx, req.Username, "login", err.Error())
		return c.JSON(resp.StatusCode(), resp)
	}

	var ok bool
	if ok, err = shared.IsRequestValid(&req); !ok {
		resp = shared.StatusBadRequest("")
		h.auditService.Insert(ctx, req.Username, "login", err.Error())
		return c.JSON(resp.StatusCode(), resp)

	}

	data, err := h.service.Login(ctx, req.Username, req.Password, h.middware)

	if err != nil {
		resp = shared.StatusInternalServerError(err.Error())
		h.auditService.Insert(ctx, req.Username, "login", err.Error())
		return c.JSON(resp.StatusCode(), resp)
	}

	h.auditService.Insert(ctx, req.Username, "login", "success")
	h.userService.UpdateLogin(ctx, req.Username, "allowed")
	resp = shared.Success(data)
	return c.JSON(resp.StatusCode(), resp)
}

// Get will fetch the item based on given params
func (h *handler) Reset(c echo.Context) error {

	type RequestReset struct {
		Username string `json:"username"`
		//Password string `json:"password"`
	}

	var req RequestReset
	var resp shared.Response
	err := c.Bind(&req)
	if err != nil {
		resp = shared.StatusUnprocessableEntity("")
		return c.JSON(resp.StatusCode(), resp)
	}

	var ok bool
	if ok, err = shared.IsRequestValid(&req); !ok {
		resp = shared.StatusBadRequest("")
		return c.JSON(resp.StatusCode(), resp)

	}

	ctx := c.Request().Context()
	err = h.service.Reset(ctx, req.Username, h.middware)

	if err != nil {
		resp = shared.StatusInternalServerError(err.Error())
		return c.JSON(resp.StatusCode(), resp)
	}
	data := ""
	resp = shared.Success(data)
	return c.JSON(resp.StatusCode(), resp)
}

// Get will fetch the item based on given params
func (h *handler) Me(c echo.Context) error {
	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {
		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		//token := c.Request().Header.Get("token")

		//user := c.Get("user").(*jwt.Token)
		//fmt.Println("================", c.Request().Header.Get("token"))

		/*username := h.middware.GetInfo(c)
		if username == "" {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)

		}
		*/
		data := ""
		//data, err := h.service.Get(ctx, username)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success(data)
		return c.JSON(resp.StatusCode(), resp)
	}
}
