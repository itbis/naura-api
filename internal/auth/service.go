package auth

import (
	"context"
	"time"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/middleware"
)

// Usecase ...
type Service interface {
	Login(ctx context.Context, username, password string, middware *middleware.GoMiddleware) (entity.User, error)
	Reset(ctx context.Context, username string, middware *middleware.GoMiddleware) error
	Get(ctx context.Context, username string) (entity.User, error)
}

type service struct {
	repo Repository
	//auditRepo      audit_trail.Repository
	contextTimeout time.Duration
}

// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
	}
}

// Login ...
func (s service) Login(ctx context.Context, username, password string, middware *middleware.GoMiddleware) (entity.User, error) {
	item, err := s.repo.Login(ctx, username, password)
	if err != nil {
		return item, err
	}

	if item.Email != "" {
		//item.Token = "qazwsx"
		token, _ := middware.CreateToken(item.Name, item.Email, item.PartnerId, item.RolesId,  middware.JwtExpired)
		item.Token = token
	}

	//s.auditRepo.Insert(ctx, c.Request().Header.Get("username"), "login", "success")

	return item, err
}

// Login ...
func (s service) Reset(ctx context.Context, username string, middware *middleware.GoMiddleware) error {
	item, err := s.repo.Get(ctx, username)
	if err != nil {
		return err
	}

	if item.Email != "" {
		//item.Token = "qazwsx"
		//res, _ := password.Generate(64, 10, 10, false, false)
		//body := "Dear " + item.Name + "\nAda perubahan password simswap dashboard : " + res
		//util.SubmitEmail()

		//from := cast.ToString(data["fromEmail"])
		//subject := cast.ToString(data["subject"])
		//content := cast.ToString(data["content"])
		//to := username
		//content = strings.Replace(content, "[link]", link, -1)
		//content = strings.Replace(body, "[link]", link, -1)

		//result := caller.SubmitEmail(uuid, from, to, subject, content)

		//logger.Debugw("SendEmail:", subject, to, content)
		//go util.SubmitEmail("uuid"+to, "noreplay@magesty.id", to, "Reset Password", body)

	}

	return err
}

// Get ...
func (s service) Get(ctx context.Context, username string) (entity.User, error) {
	item, err := s.repo.Get(ctx, username)
	if err != nil {
		return item, err
	}

	return item, err
}
