package auth

import (
	"context"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access users from the data source.
type Repository interface {
	// Get returns the user with the specified user ID.
	Get(ctx context.Context, id string) (entity.User, error)
	Login(ctx context.Context, username, password string) (entity.User, error)
}

// repository persists users in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new user repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {

	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.User, error) {
	var user entity.User
	err := r.db.First(&user, "email=?", id).Error

	return user, err
}

// Get
func (r repository) Login(ctx context.Context, username, password string) (entity.User, error) {
	var user entity.User

	//query := "select * from users where email=? and password=md5(?)"
	//r.logger.Syslog.Debug("SQL", zap.Any("query", query), zap.Any("p1", username), zap.Any("p2", password))
	//err := r.db.Get(&user, query, username, password)
	err := r.db.First(&user, "email=? and password=md5(?)", username, password).Error
	return user, err
}
