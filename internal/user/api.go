package user

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/itbis/naura-api/internal/audit_trail"
	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/middleware"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/pagination"
	"gitlab.com/itbis/naura-api/shared"
)

// Handler  represent the httphandler
type handler struct {
	service      Service
	auditService audit_trail.Service
	logger       logger.Logger
	middware     *middleware.GoMiddleware
}

// RegisterHandler ...
func RegisterHandler(e *echo.Echo, service Service, auditService audit_trail.Service, logger logger.Logger, middware *middleware.GoMiddleware) {
	h := &handler{
		service:      service,
		auditService: auditService,
		logger:       logger,
		middware:     middware,
	}
	v := e.Group("/naura/v1")
	v.GET("/users/:id", h.Get, h.middware.IsLoggedIn, h.middware.IsAdmin, h.middware.SetHeader)
	v.GET("/users", h.Query, h.middware.IsLoggedIn, h.middware.IsAdmin,  h.middware.SetHeader)
	v.POST("/users", h.Post, h.middware.IsLoggedIn, h.middware.IsAdmin, h.middware.SetHeader)
	v.PUT("/users/:id", h.Put, h.middware.IsLoggedIn, h.middware.IsAdmin, h.middware.SetHeader)
	v.DELETE("/users/:id", h.Delete, h.middware.IsLoggedIn, h.middware.IsAdmin, h.middware.SetHeader)
	v.GET("/users/logout", h.Logout, h.middware.IsLoggedIn, h.middware.IsAdmin, h.middware.SetHeader)

}

type Request struct {
}

// Get will fetch the item based on given params
func (h *handler) Get(c echo.Context) error {
	var req Request
	var resp shared.Response
	user := h.middware.GetInfo(c)
	getCurrentUser, _ := h.service.CheckEmailUser(user.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")
		data, err := h.service.Get(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "get/users", "success")

		resp = shared.Success(data)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Query(c echo.Context) error {


	var req Request
	var resp shared.Response
	user := h.middware.GetInfo(c)
	getCurrentUser, _ := h.service.CheckEmailUser(user.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		user := h.middware.GetInfo(c)
		getCurrentUser, _ := h.service.CheckEmailUser(user.Email)
		fmt.Println(getCurrentUser.Blacklist)
		if getCurrentUser.Blacklist == "disallowed" {
			resp = shared.StatusForbidden("")
			return c.JSON(resp.StatusCode(), resp)
		} else {
			err := c.Bind(&req)
			if err != nil {
				resp = shared.StatusUnprocessableEntity("")
				return c.JSON(resp.StatusCode(), resp)
			}

			var ok bool
			if ok, err = shared.IsRequestValid(&req); !ok {
				resp = shared.StatusBadRequest("")
				return c.JSON(resp.StatusCode(), resp)

			}

			ctx := c.Request().Context()
			count, err := h.service.Count(ctx)
			if err != nil {
				resp = shared.StatusNotFound("")
				return c.JSON(resp.StatusCode(), resp)
			}
			pages := pagination.NewFromRequest(c.Request(), count)
			data, err := h.service.Query(ctx, pages.Offset(), pages.Limit())

			if err != nil {
				resp = shared.StatusInternalServerError(err.Error())
				return c.JSON(resp.StatusCode(), resp)
			}
			h.auditService.Insert(ctx, c.Request().Header.Get("username"), "query/users", "success")

			pages.Items = data
			resp = shared.Success(pages)
			return c.JSON(resp.StatusCode(), resp)
		}
	}
}

// Get will fetch the item based on given params
func (h *handler) Post(c echo.Context) error {

	var req entity.User
	var checkPartner entity.Partner
	var resp shared.Response

	user := h.middware.GetInfo(c)
	getCurrentUser, _ := h.service.CheckEmailUser(user.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		errPartner := c.Bind(&checkPartner)

		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		err = req.Validate()
		if err != nil {
			resp = shared.StatusBadRequest(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)
		}

		dataPartner, errPartner := h.service.CheckPartner(req.PartnerId)
		if errPartner != nil {
			resp = shared.StatusInternalServerError(errPartner.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		if dataPartner.Name != req.PartnerId {
			resp = shared.StatusBadRequest("Partner " + req.PartnerId + " Not Found")
			return c.JSON(resp.StatusCode(), resp)
		} else {
			ctx := c.Request().Context()
			_, i, _ := h.service.CheckEmail(ctx, req.Email)
			if i == 1 {
				resp = shared.StatusBadRequest("Email Existed")
				return c.JSON(resp.StatusCode(), resp)
			} else {
				err = h.service.Create(ctx, req)
				if err != nil {
					resp = shared.StatusInternalServerError(err.Error())
					return c.JSON(resp.StatusCode(), resp)
				}
				h.auditService.Insert(ctx, c.Request().Header.Get("username"), "post/users", "success")
				resp = shared.Success("")
				return c.JSON(resp.StatusCode(), resp)
			}
		}

		/*ctx := c.Request().Context()
		err = h.service.Create(ctx, req)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "post/users", "success")

		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)*/
	}
}

// Get will fetch the item based on given params
func (h *handler) Put(c echo.Context) error {

	var req entity.User
	var resp shared.Response
	user := h.middware.GetInfo(c)
	getCurrentUser, _ := h.service.CheckEmailUser(user.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)

		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		err = req.Validate()
		if err != nil {
			resp = shared.StatusBadRequest(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		err = h.service.Update(ctx, c.Param("id"), req)

		dataPartner, errPartner := h.service.CheckPartner(req.PartnerId)
		if errPartner != nil {
			resp = shared.StatusInternalServerError(errPartner.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		if dataPartner.Name != req.PartnerId {
			resp = shared.StatusBadRequest("Partner " + req.PartnerId + " Not Found")
			return c.JSON(resp.StatusCode(), resp)
		} else {
			ctx := c.Request().Context()
			data, err := h.service.Get(ctx, c.Param("id"))
			if err != nil {
				resp = shared.StatusInternalServerError(err.Error())
				return c.JSON(resp.StatusCode(), resp)
			}
			if data.Email == req.Email {
				err = h.service.Update(ctx, c.Param("id"), req)
				if err != nil {
					resp = shared.StatusInternalServerError(err.Error())
					return c.JSON(resp.StatusCode(), resp)
				}
				resp = shared.Success("")
				return c.JSON(resp.StatusCode(), resp)
			} else {
				_, i, err := h.service.CheckEmail(ctx, req.Email)
				if err != nil {
					resp = shared.StatusInternalServerError(err.Error())
					return c.JSON(resp.StatusCode(), resp)
				}
				if i == 1 {
					resp = shared.StatusBadRequest("Email Exist")
					return c.JSON(resp.StatusCode(), resp)
				} else {
					err = h.service.Update(ctx, c.Param("id"), req)
					if err != nil {
						resp = shared.StatusInternalServerError(err.Error())
						return c.JSON(resp.StatusCode(), resp)
					}
					h.auditService.Insert(ctx, c.Request().Header.Get("username"), "update/users", "success")
					resp = shared.Success("")
					return c.JSON(resp.StatusCode(), resp)
				}
			}
		}
		/*	if err != nil {
				resp = shared.StatusInternalServerError(err.Error())
				return c.JSON(resp.StatusCode(), resp)
			}
			h.auditService.Insert(ctx, c.Request().Header.Get("username"), "update/users", "success")

			resp = shared.Success("")
			return c.JSON(resp.StatusCode(), resp)*/
	}
}

// Delete
func (h *handler) Delete(c echo.Context) error {

	var req entity.User
	var resp shared.Response
	user := h.middware.GetInfo(c)
	getCurrentUser, _ := h.service.CheckEmailUser(user.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")
		err = h.service.Delete(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "delete/users", "success")

		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}


// Get Logout will fetch the item based on given params
func (h *handler) Logout(c echo.Context) error {
	var req Request
	var resp shared.Response
	user := h.middware.GetInfo(c)
	getCurrentUser, _ := h.service.CheckEmailUser(user.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		err = h.service.UpdateLogin(ctx, c.Request().Header.Get("username"), "disallowed")
		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "logout/users", "success")
		resp = shared.Success("Successfully Logout")
		return c.JSON(resp.StatusCode(), resp)
	}
}
