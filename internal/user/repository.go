package user

import (
	"context"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/util"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access users from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.User, error)
	Count(ctx context.Context) (int, error)
	Query(ctx context.Context, offset, limit int) ([]entity.User, error)
	Create(ctx context.Context, user entity.User) error
	Update(ctx context.Context, id string, user entity.User) error
	Delete(ctx context.Context, id string) error
	GetPartner(name string) (partner entity.Partner, err error)
	CheckEmail(ctx context.Context, email string) (entity.User, int, error)
	UpdateLogin(ctx context.Context, username string, isBlacklist string) error
	CheckBlackList(email string) (entity.User, int)
	CheckEmailUser(email string) (entity.User, int)
}

// repository persists users in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}


// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.User, error) {
	var user entity.User

	err := r.db.Table("users").First(&user, "id=?", id).Error
	user.Password = ""
	user.CreatedAt = util.FormatDate(user.CreatedAt)
	user.UpdatedAt = util.FormatDate(user.UpdatedAt)
	return user, err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.User, error) {
	var users []entity.User
	err := r.db.Table("users").Find(&users).Offset(offset).Limit(limit).Error
	for i := range users {
		users[i].Password = "-"
		users[i].CreatedAt = util.FormatDate(users[i].CreatedAt)
		users[i].UpdatedAt = util.FormatDate(users[i].UpdatedAt)
	}
	return users, err
}

// count
func (r repository) Count(ctx context.Context) (int, error) {
	var user entity.User
	var count int64
	result := r.db.Table("users").Find(&user).Count(&count)
	return int(count), result.Error
}

// Create saves a new user record in the database.
func (r repository) Create(ctx context.Context, user entity.User) error {

	user.Password = util.GetMD5Hash(user.Password)
	user.Status = "active"
	user.Blacklist = "disallowed"
	return r.db.Table("users").Create(&user).Error
}

// Update ...
func (r repository) Update(ctx context.Context, id string, userNew entity.User) error {
	var user entity.User
	err := r.db.Table("users").First(&user, "id=?", id).Error
	if err != nil {
		return err
	}
	if userNew.Password != "" {
		userNew.Password = util.GetMD5Hash(userNew.Password)
	}
	err = r.db.Table("users").Where("id = ?", id).Updates(userNew).Error
	return err

}

// Delete ...
func (r repository) Delete(ctx context.Context, id string) error {
	var user entity.User
	err := r.db.Table("users").First(&user, "id=?", id).Error
	if err != nil {
		return err
	}
	err = r.db.Table("users").Where("id = ?", id).Delete(user).Error
	return err
}

func (r repository) GetPartner(name string) (entity.Partner, error) {
	var partner entity.Partner
	err := r.db.Table("partner").First(&partner, "name=?", name).Error
	return partner, err
}

func (r repository) CheckEmail(ctx context.Context, email string) (entity.User, int, error) {
	var user entity.User
	result := r.db.Table("users").First(&user, "email=?", email)
	return user, int(result.RowsAffected), result.Error
}

func (r repository) UpdateLogin(ctx context.Context, username string, isBlacklist string) error {
	var user entity.User
	/*auditTrail := entity.AuditTrail{Username: username, Action: action, Desc: desc}
	result := r.db.Create(&auditTrail)
	return result.Error*/
	err := r.db.Raw("UPDATE users SET blacklist = ? WHERE email = ?", isBlacklist, username).Scan(&user).Error
	return err
}

func (r repository) CheckBlackList(blacklist string) (entity.User, int) {
	var user entity.User
	result := r.db.Table("users").First(&user, "blacklist=?", blacklist)
	return user, int(result.RowsAffected)
}

func (r repository) CheckEmailUser(email string) (entity.User, int) {
	var user entity.User
	result := r.db.Table("users").First(&user, "email=?", email)
	return user, int(result.RowsAffected)
}
