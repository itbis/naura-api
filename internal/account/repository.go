package account

import (
	"context"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/util"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access menus from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.PartnerAccount, error)
	Count(ctx context.Context) (int, error)
	Query(ctx context.Context, offset, limit int) ([]entity.PartnerAccount, error)
	Create(ctx context.Context, item entity.PartnerAccount) error
	Update(ctx context.Context, id string, item entity.PartnerAccount) error
	Delete(ctx context.Context, id string) error
}

// repository persists menus in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.PartnerAccount, error) {
	var item entity.PartnerAccount
	err := r.db.Table("partner_account").First(&item, "id=?", id).Error
	return item, err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.PartnerAccount, error) {
	var menus []entity.PartnerAccount
	err := r.db.Table("partner_account").Find(&menus).Offset(offset).Limit(limit).Error
	return menus, err
}

// count
func (r repository) Count(ctx context.Context) (int, error) {
	var item entity.PartnerAccount
	result := r.db.Table("partner_account").Find(&item)
	return int(result.RowsAffected), result.Error
}

// Create saves a new item record in the database.
func (r repository) Create(ctx context.Context, item entity.PartnerAccount) error {
	if item.Msisdn != "" {
		item.MsisdnHash = util.GetHash1(item.Msisdn)
	}
	//insert partner_request
	response := "OK"
	sql := "INSERT INTO partner_request (`partner_id`,`transaction_id`,`msisdn`,`msisdn_hash`,`action`,`response`) VALUES (?,?,?,?,?,?)"
	err := r.db.Exec(sql, item.PartnerId, item.TransactionId, item.Msisdn, item.MsisdnHash, item.Action, response).Error
	if err != nil {
		return err
	}
	//insert partner_account
	isActive := 0
	if item.Action == "register" {
		isActive = 1
	}
	sql = "INSERT INTO partner_account (`partner_id`,`msisdn`,`msisdn_hash`,`is_active`,`channel`) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE is_active=?, counter=counter+1, updated_at=NOW() "
	err = r.db.Exec(sql, item.PartnerId, item.Msisdn, item.MsisdnHash, isActive, item.PartnerId, isActive).Error

	return err
}

// Update ...
func (r repository) Update(ctx context.Context, id string, userNew entity.PartnerAccount) error {
	var item entity.PartnerAccount
	err := r.db.Table("partner_account").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}

	err = r.db.Table("partner_account").Where("id = ?", id).Updates(userNew).Error
	return err

}

// Delete ...
func (r repository) Delete(ctx context.Context, id string) error {
	var item entity.PartnerAccount
	err := r.db.Table("partner_account").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}
	err = r.db.Table("partner_account").Where("id = ?", id).Delete(item).Error
	return err
}
