package partner

import (
	"context"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/util"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access menus from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.Partner, error)
	Count(ctx context.Context) (int, error)
	Query(ctx context.Context, offset, limit int) ([]entity.Partner, error)
	Create(ctx context.Context, item entity.Partner) error
	Update(ctx context.Context, id string, item entity.Partner) error
	Delete(ctx context.Context, id string) error
	Exist(name string) (int, error)
	CheckPartner(name string) (entity.Partner, error)
}

// repository persists menus in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.Partner, error) {
	var item entity.Partner
	err := r.db.Table("partner").First(&item, "id=?", id).Error
	item.CreatedAt = util.FormatDate(item.CreatedAt)
	return item, err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.Partner, error) {
	var partners []entity.Partner
	err := r.db.Table("partner").Find(&partners).Offset(offset).Limit(limit).Error
	for i := range partners {
		partners[i].CreatedAt = util.FormatDate(partners[i].CreatedAt)
	}

	return partners, err
}

// count
func (r repository) Count(ctx context.Context) (int, error) {
	var item entity.Partner
	result := r.db.Table("partner").Find(&item)
	return int(result.RowsAffected), result.Error
}

// Create saves a new item record in the database.
func (r repository) Create(ctx context.Context, item entity.Partner) error {
	return r.db.Table("partner").Create(&item).Error
}

// Update ...
func (r repository) Update(ctx context.Context, id string, userNew entity.Partner) error {
	var item entity.Partner
	err := r.db.Table("partner").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}

	err = r.db.Table("partner").Where("id = ?", id).Updates(userNew).Error
	return err

}

// Delete ...
func (r repository) Delete(ctx context.Context, id string) error {
	var item entity.Partner
	err := r.db.Table("partner").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}
	err = r.db.Table("partner").Where("id = ?", id).Delete(item).Error
	return err
}


func (r repository) Exist(name string) (int, error) {
	var item entity.Partner
	result := r.db.Table("partner").First(&item, "name=?", name)
	return int(result.RowsAffected), result.Error
}

func (r repository) CheckPartner(name string) (entity.Partner, error) {
	var item entity.Partner
	err := r.db.Table("partner").First(&item, "name=?", name).Error
	return item, err
}
