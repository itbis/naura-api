package partner

import (
	"context"
	"time"

	"gitlab.com/itbis/naura-api/internal/entity"
)

// Usecase ...
type Service interface {
	Get(ctx context.Context, id string) (entity.Partner, error)
	Query(ctx context.Context, offset, limit int) ([]entity.Partner, error)
	Count(ctx context.Context) (int, error)
	Create(ctx context.Context, item entity.Partner) error
	Update(ctx context.Context, id string, item entity.Partner) error
	Delete(ctx context.Context, id string) error
	Exist(name string) (int, error)
	CheckPartner(name string) (entity.Partner, error)
}

type service struct {
	repo           Repository
	contextTimeout time.Duration
}

// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
	}
}

// Count ...
func (s service) Count(ctx context.Context) (int, error) {
	return s.repo.Count(ctx)
}

// Get ...
func (s service) Get(ctx context.Context, id string) (entity.Partner, error) {
	item, err := s.repo.Get(ctx, id)
	if err != nil {
		return item, err
	}

	return item, err
}

// Query ...
func (s service) Query(ctx context.Context, offset, limit int) ([]entity.Partner, error) {
	items, err := s.repo.Query(ctx, offset, limit)
	if err != nil {
		return items, err
	}

	return items, err
}

// Get ...
func (s service) Create(ctx context.Context, item entity.Partner) error {
	err := s.repo.Create(ctx, item)
	return err
}

func (s service) Update(ctx context.Context, id string, item entity.Partner) error {
	err := s.repo.Update(ctx, id, item)
	return err
}

func (s service) Delete(ctx context.Context, id string) error {
	err := s.repo.Delete(ctx, id)
	return err
}

func (s service) Exist(email string) (int, error) {
	return s.repo.Exist(email)
}

func (s service) CheckPartner(name string) (entity.Partner, error) {
	return s.repo.CheckPartner(name)
}
