package partner

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/itbis/naura-api/internal/audit_trail"
	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/internal/user"
	"gitlab.com/itbis/naura-api/middleware"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/pagination"
	"gitlab.com/itbis/naura-api/shared"
)


// Handler  represent the httphandler
type handler struct {
	service      Service
	auditService audit_trail.Service
	userService  user.Service
	logger       logger.Logger
	middware     *middleware.GoMiddleware
}

// RegisterHandler ...
func RegisterHandler(e *echo.Echo,
	service Service,
	auditService audit_trail.Service,
	userService user.Service,
	logger logger.Logger,
	middware *middleware.GoMiddleware) {
	h := &handler{
		service:      service,
		auditService: auditService,
		userService: userService,
		logger:       logger,
		middware:     middware,
	}

	v := e.Group("/naura/v1")
	v.GET("/partner/:id", h.Get, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.GET("/partner", h.Query, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.POST("/partner", h.Post, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.PUT("/partner/:id", h.Put, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.DELETE("/partner/:id", h.Delete, h.middware.IsLoggedIn, h.middware.SetHeader)

}

type Request struct {
	PartnerId string `json:"partner_id"`
}


// Get will fetch the item based on given params
func (h *handler) Get(c echo.Context) error {

	var req Request
	var resp shared.Response
	//req.PartnerId = c.Request().Header.Get("partnerId")
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")

		data, err := h.service.Get(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success(data)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Query(c echo.Context) error {

	var req Request
	var resp shared.Response
	//req.PartnerId = c.Request().Header.Get("partnerId")
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		count, err := h.service.Count(ctx)
		if err != nil {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)
		}
		pages := pagination.NewFromRequest(c.Request(), count)
		data, err := h.service.Query(ctx, pages.Offset(), pages.Limit())

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		pages.Items = data

		resp = shared.Success(pages)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Post(c echo.Context) error {

	var req entity.Partner
	var resp shared.Response
	//req.PartnerId = c.Request().Header.Get("partnerId")
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)
		}

		err = req.Validate()
		if err != nil {
			resp = shared.StatusBadRequest(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		exist, _ := h.service.Exist(req.Name)
		if exist == 1 {
			resp = shared.StatusBadRequest("Data Existed")
			return c.JSON(resp.StatusCode(), resp)
		}

		ctx := c.Request().Context()
		err = h.service.Create(ctx, req)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Put(c echo.Context) error {

	var req entity.Partner
	var resp shared.Response
	//req.PartnerId = c.Request().Header.Get("partnerId")
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)
		}

		err = req.Validate()
		if err != nil {
			resp = shared.StatusBadRequest(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		/*ctx := c.Request().Context()
		id := c.Param("id")
		err = h.service.Update(ctx, id, req)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)*/

		ctx := c.Request().Context()
		id := c.Param("id")
		data, err := h.service.Get(ctx, id)
		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		if data.Name == req.Name {
			err = h.service.Update(ctx, id, req)
			if err != nil {
				resp = shared.StatusInternalServerError(err.Error())
				return c.JSON(resp.StatusCode(), resp)
			}
			resp = shared.Success("")
			return c.JSON(resp.StatusCode(), resp)
		} else {
			exist, err := h.service.Exist(req.Name)
			if exist == 1 {
				resp = shared.StatusBadRequest("Data Existed")
				return c.JSON(resp.StatusCode(), resp)
			} else {
				err = h.service.Update(ctx, id, req)
				if err != nil {
					resp = shared.StatusInternalServerError(err.Error())
					return c.JSON(resp.StatusCode(), resp)
				}
				resp = shared.Success("")
				return c.JSON(resp.StatusCode(), resp)
			}
		}
	}
}

// Delete
func (h *handler) Delete(c echo.Context) error {

	var req entity.Partner
	var resp shared.Response
	//req.PartnerId = c.Request().Header.Get("partnerId")
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)
		}

		ctx := c.Request().Context()
		id := c.Param("id")
		err = h.service.Delete(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}
