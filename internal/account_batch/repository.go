package account_batch

import (
	"context"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/util"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access menus from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.AccountBatch, error)
	Count(ctx context.Context) (int, error)
	Query(ctx context.Context, offset, limit int) ([]entity.AccountBatch, error)
	Create(ctx context.Context, item entity.AccountBatch) (int, error)
	Update(ctx context.Context, id string, item entity.AccountBatch) error
	Delete(ctx context.Context, id string) error
	Insert(ctx context.Context, partnerId, msisdn string, isActive int) error
	Done(ctx context.Context, id int) error
}

// repository persists menus in database
type repository struct {
	db      *gorm.DB
	dbNaura *gorm.DB
	logger  logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, dbNaura *gorm.DB, logger logger.Logger) Repository {
	return repository{db, dbNaura, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.AccountBatch, error) {
	var item entity.AccountBatch
	err := r.db.Table("account_batch").First(&item, "id=?", id).Error
	return item, err
}

// Insert
func (r repository) Insert(ctx context.Context, partnerId, msisdn string, isActive int) error {

	msisdnHash := util.GetHash1(msisdn)
	sql := "INSERT INTO partner_account (`partner_id`,`msisdn`,`msisdn_hash`,`is_active`,`channel`) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE is_active=?, counter=counter+1, updated_at=NOW() "
	tx := r.dbNaura.Exec(sql, partnerId, msisdn, msisdnHash, isActive, partnerId, isActive)
	/*var item entity.AccountBatch
	id := r.db.Table("account_batch").Last(&item)
	sql = "UPDATE account_batch set status=1 where id=?"
	r.db.Exec(sql, id)
	*/
	return tx.Error
}

// Insert
func (r repository) Done(ctx context.Context, id int) error {

	sql := "UPDATE account_batch set status=1 where id=?"
	err := r.db.Exec(sql, id).Error

	return err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.AccountBatch, error) {
	var menus []entity.AccountBatch
	err := r.db.Table("account_batch").Find(&menus).Offset(offset).Limit(limit).Error
	return menus, err
}

// count
func (r repository) Count(ctx context.Context) (int, error) {
	var item entity.AccountBatch
	result := r.db.Table("account_batch").Find(&item)
	return int(result.RowsAffected), result.Error
}

// Create saves a new item record in the database.
func (r repository) Create(ctx context.Context, item entity.AccountBatch) (int, error) {
	trx := r.db.Table("account_batch").Create(&item)

	return item.ID, trx.Error
}

// Update ...
func (r repository) Update(ctx context.Context, id string, userNew entity.AccountBatch) error {
	var item entity.AccountBatch
	err := r.db.Table("account_batch").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}

	err = r.db.Table("account_batch").Where("id = ?", id).Updates(userNew).Error
	return err

}

// Delete ...
func (r repository) Delete(ctx context.Context, id string) error {
	var item entity.AccountBatch
	err := r.db.Table("account_batch").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}
	err = r.db.Table("account_batch").Where("id = ?", id).Delete(item).Error
	return err
}
