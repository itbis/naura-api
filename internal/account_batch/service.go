package account_batch

import (
	"bufio"
	"context"
	"os"
	"time"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"go.uber.org/zap"
)

// Usecase ...
type Service interface {
	Get(ctx context.Context, id string) (entity.AccountBatch, error)
	Query(ctx context.Context, offset, limit int) ([]entity.AccountBatch, error)
	Count(ctx context.Context) (int, error)
	Create(ctx context.Context, item entity.AccountBatch) (int, error)
	Update(ctx context.Context, id string, item entity.AccountBatch) error
	Delete(ctx context.Context, id string) error
	ProcessFile(ctx context.Context, partnerId, pathFile string, isActive int, idBatch int)
}

type service struct {
	repo           Repository
	contextTimeout time.Duration
	logger         logger.Logger
}

// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration, logger logger.Logger) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
		logger:         logger,
	}
}

func (s service) ProcessFile(ctx context.Context, partnerId, pathFile string, isActive int, idBatch int) {

	openFile, err := os.Open(pathFile)
	if err != nil {
		s.logger.Syslog.Debug("error", zap.Any("openFile", err))
	}
	//read
	scanner := bufio.NewScanner(openFile)
	scanner.Split(bufio.ScanLines)
	defer openFile.Close()

	for scanner.Scan() {
		line := scanner.Text()
		if line != "" {
			s.logger.Syslog.Debug("content", zap.Any("line", line))
			s.repo.Insert(ctx, partnerId, line, isActive)

		}
		s.repo.Done(ctx, idBatch)
	}

}

// Count ...
func (s service) Count(ctx context.Context) (int, error) {
	return s.repo.Count(ctx)
}

// Get ...
func (s service) Get(ctx context.Context, id string) (entity.AccountBatch, error) {
	item, err := s.repo.Get(ctx, id)
	if err != nil {
		return item, err
	}

	return item, err
}

// Query ...
func (s service) Query(ctx context.Context, offset, limit int) ([]entity.AccountBatch, error) {
	items, err := s.repo.Query(ctx, offset, limit)
	if err != nil {
		return items, err
	}

	return items, err
}

// Get ...
func (s service) Create(ctx context.Context, item entity.AccountBatch) (int, error) {
	id, err := s.repo.Create(ctx, item)

	return id, err
}

func (s service) Update(ctx context.Context, id string, item entity.AccountBatch) error {
	err := s.repo.Update(ctx, id, item)
	return err
}

func (s service) Delete(ctx context.Context, id string) error {
	err := s.repo.Delete(ctx, id)
	return err
}
