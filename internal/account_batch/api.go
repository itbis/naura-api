package account_batch

import (
	"context"
	"fmt"
	"gitlab.com/itbis/naura-api/internal/user"
	"io"
	"os"

	"github.com/labstack/echo/v4"
	"gitlab.com/itbis/naura-api/internal/audit_trail"
	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/middleware"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/pagination"
	"gitlab.com/itbis/naura-api/shared"
)

// Handler  represent the httphandler
type handler struct {
	service      Service
	auditService audit_trail.Service
	userService  user.Service
	logger       logger.Logger
	middware     *middleware.GoMiddleware
}

// RegisterHandler ...
func RegisterHandler(e *echo.Echo, service Service, auditService audit_trail.Service, userService user.Service, logger logger.Logger, middware *middleware.GoMiddleware) {
	h := &handler{
		service:      service,
		auditService: auditService,
		userService: userService,
		logger:       logger,
		middware:     middware,
	}

	v := e.Group("/naura/v1")
	v.GET("/account-batch/:id", h.Get, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.GET("/account-batch", h.Query, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.POST("/account-batch", h.Post, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.PUT("/account-batch/:id", h.Put, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.DELETE("/account-batch/:id", h.Delete, h.middware.IsLoggedIn, h.middware.SetHeader)

}

type Request struct {
	PartnerId string
}


// Get will fetch the item based on given params
func (h *handler) Get(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		req.PartnerId = c.Request().Header.Get("partnerId")

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")

		data, err := h.service.Get(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "get/batch", "success")

		resp = shared.Success(data)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Query(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		req.PartnerId = c.Request().Header.Get("partnerId")

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		count, err := h.service.Count(ctx)
		if err != nil {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)
		}
		pages := pagination.NewFromRequest(c.Request(), count)
		data, err := h.service.Query(ctx, pages.Offset(), pages.Limit())

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		pages.Items = data

		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "query/batch", "success")
		resp = shared.Success(pages)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Post(c echo.Context) error {

	var req entity.AccountBatch
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {
		req.PartnerId = c.Request().Header.Get("partnerId")

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		////////////////////upload///////////////////////////////////
		file, err := c.FormFile("file")
		if err != nil {
			resp = shared.StatusBadRequest(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		src, err := file.Open()
		if err != nil {
			resp = shared.StatusBadRequest(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		defer src.Close()

		// Destination
		pathFile := "uploads" + "/" + file.Filename
		dst, err := os.Create(pathFile)
		if err != nil {
			resp = shared.StatusBadRequest(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		defer dst.Close()
		// Copy
		if _, err = io.Copy(dst, src); err != nil {
			resp = shared.StatusBadRequest(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		/////////////////////////////////////////
		req.File = file.Filename
		req.PartnerId = c.Request().Header.Get("partnerId")
		isActive := 1
		if req.Action != "register" {
			req.Action = "unregister"
			isActive = 0
		}
		//////////////////////////////////////////

		ctx := c.Request().Context()
		idBatch, err := h.service.Create(ctx, req)
		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		//idBatch := 0
		//HANDLE ASYNC PROCESS BULK//
		go func(ctx context.Context) {
			h.service.ProcessFile(ctx, req.PartnerId, pathFile, isActive, idBatch)

		}(ctx)

		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "post/batch", "success")

		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Put(c echo.Context) error {

	var req entity.AccountBatch
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {
		req.PartnerId = c.Request().Header.Get("partnerId")

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")
		err = h.service.Update(ctx, id, req)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Delete
func (h *handler) Delete(c echo.Context) error {

	var req entity.AccountBatch
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {
		req.PartnerId = c.Request().Header.Get("partnerId")

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)
		}

		ctx := c.Request().Context()
		id := c.Param("id")
		err = h.service.Delete(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}
