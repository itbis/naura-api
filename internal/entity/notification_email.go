package entity

import "time"

// Notification
type NotificationEmail struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	PartnerId string `json:"partner_id"`
	EventType string `json:"eventType" validate:"required"`
	Subject   string `json:"subject"`
	//Template   string    `json:"template"`
	To        string    `json:"to"`
	Cc        string    `json:"cc"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	IsActive  int       `json:"is_active"`
}
