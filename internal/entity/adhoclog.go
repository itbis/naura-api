package entity

// User represents a BidLoan.
type Adhoclog struct {
	ID           string  `db:"pk" json:"id"`
	Msisdn       string  `json:"msisdn"`
	Denom        int     `json:"denom"`
	BidLoan      string  `json:"bid_loan"`
	CampaignID   string  `json:"campaign_id"`
	SmsResp      string  `json:"sms_resp"`
	UrlDeductRes *string `json:"url_deduct_res"`
	CreatedTime  string  `json:"created_time"`
	UpdatedTime  string  `json:"updated_time"`
}

// get table real
func (c Adhoclog) TableName() string {
	return "adhoc_log"
}
