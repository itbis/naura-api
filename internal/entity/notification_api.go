package entity

import "time"

// Notification
type NotificationAPI struct {
	ID              int       `json:"id"`
	Username        string    `json:"username"`
	PartnerId       string    `json:"partner_id"`
	URLToken        string    `json:"url_token"`
	URLNotification string    `json:"url_notification"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
	Parameters      string    `json:"parameters"`
	IsActive        int       `json:"is_active" `
}
