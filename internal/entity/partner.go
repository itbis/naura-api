package entity

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"time"
)

// Notification
type Partner struct {
	ID        int       `json:"id"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
}

func (c Partner) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Name, validation.Required),
	)
}