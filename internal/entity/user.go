package entity

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"time"
)

// User represents a user.
type User struct {
	ID          int       `json:"id"`
	Name        string    `json:"name" validate:"required"`
	Email       string    `json:"email" validate:"required"`
	RolesId     int       `json:"roles_id"`
	Photo       string    `json:"photo"`
	PartnerId   string    `json:"partner_id" validate:"required"`
	Status      string    `json:"status"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Password    string    `json:"password" gorm:"->:false;<-:create:update"`
	Token       string    `json:"token,omitempty" gorm:"-"`
	Blacklist   string    `json:"blacklist"`
}

func (c User) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Name, validation.Required),
		validation.Field(&c.Email, validation.Required, is.Email),
		validation.Field(&c.RolesId, validation.Required),
		validation.Field(&c.PartnerId, validation.Required),
		validation.Field(&c.Password, validation.Required),
	)
}

func (c *User) SetBlackList(blackList string) {
	c.Blacklist = blackList
}

func (c User) GetBlackList() string {
	return c.Blacklist
}