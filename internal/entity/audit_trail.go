package entity

import "time"

type AuditTrail struct {
	ID        int       `json:"id"`
	Username  string    `json:"username"`
	RolesId   int       `json:"roles_id"`
	Action    string    `json:"action"`
	Desc      string    `json:"desc"`
	CreatedAt time.Time `json:"created_at"`
}
