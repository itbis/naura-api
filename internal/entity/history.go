package entity

import "time"

type HistoryPartner struct {
	PartnerId     string    `json:"partner_id"`
	TransactionId string    `json:"transaction_id"`
	CreatedAt     time.Time `json:"created_at"`
	Action        string    `json:"action"`
}
type History struct {
	Msisdn         string           `json:"msisdn"`
	PartnerId      string           `json:"partner_id"`
	IsActive       bool             `json:"is_active"`
	CreatedAt      time.Time        `json:"created_at"`
	UpdatedAt      time.Time        `json:"updated_at"`
	HistorySimswap []string         `json:"history_simswap" gorm:"-"`
	HistoryRecycle []string         `json:"history_recycle" gorm:"-"`
	HistoryPartner []HistoryPartner `json:"history_partner" gorm:"-"`
}
