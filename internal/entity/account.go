package entity

import "time"

// User represents a user.
type PartnerAccount struct {
	ID            int    `json:"id"`
	PartnerId     string `json:"partner_id"`
	Msisdn        string `json:"msisdn"`
	MsisdnHash    string `json:"msisdn_hash"`
	TransactionId string `json:"transaction_id"`
	Action        string `json:"action" validate:"required"`
}

// User represents a user.
type AccountBatch struct {
	ID        int       `json:"id"`
	PartnerId string    `json:"partner_id"`
	Name      string    `json:"name" form:"name"`
	File      string    `json:"file" form:"file"`
	Action    string    `json:"action" form:"action"`
	Status    int       `json:"status"`
	CreatedAt time.Time `json:"created_at"`
}
