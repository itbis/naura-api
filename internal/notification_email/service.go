package notification_email

import (
	"context"
	"time"

	"gitlab.com/itbis/naura-api/internal/entity"
)

// Usecase ...
type Service interface {
	Get(ctx context.Context, id string) (entity.NotificationEmail, error)
	Query(ctx context.Context, offset, limit int) ([]entity.NotificationEmail, error)
	Count(ctx context.Context) (int, error)
	Create(ctx context.Context, item entity.NotificationEmail) error
	Update(ctx context.Context, id string, item entity.NotificationEmail) error
	Delete(ctx context.Context, id string) error
}

type service struct {
	repo           Repository
	contextTimeout time.Duration
}

// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
	}
}

// Count ...
func (s service) Count(ctx context.Context) (int, error) {
	return s.repo.Count(ctx)
}

// Get ...
func (s service) Get(ctx context.Context, id string) (entity.NotificationEmail, error) {
	item, err := s.repo.Get(ctx, id)
	if err != nil {
		return item, err
	}

	return item, err
}

// Query ...
func (s service) Query(ctx context.Context, offset, limit int) ([]entity.NotificationEmail, error) {
	items, err := s.repo.Query(ctx, offset, limit)
	if err != nil {
		return items, err
	}

	return items, err
}

// Get ...
func (s service) Create(ctx context.Context, item entity.NotificationEmail) error {
	err := s.repo.Create(ctx, item)
	return err
}

func (s service) Update(ctx context.Context, id string, item entity.NotificationEmail) error {
	err := s.repo.Update(ctx, id, item)
	return err
}

func (s service) Delete(ctx context.Context, id string) error {
	err := s.repo.Delete(ctx, id)
	return err
}
