package notification_email

import (
	"context"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/util"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access menus from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.NotificationEmail, error)
	Count(ctx context.Context) (int, error)
	Query(ctx context.Context, offset, limit int) ([]entity.NotificationEmail, error)
	Create(ctx context.Context, item entity.NotificationEmail) error
	Update(ctx context.Context, id string, item entity.NotificationEmail) error
	Delete(ctx context.Context, id string) error
}

// repository persists menus in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.NotificationEmail, error) {
	var item entity.NotificationEmail
	err := r.db.Table("notification_email").First(&item, "id=?", id).Error
	item.CreatedAt = util.FormatDate(item.CreatedAt)
	item.UpdatedAt = util.FormatDate(item.UpdatedAt)

	return item, err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.NotificationEmail, error) {
	var notifs []entity.NotificationEmail
	err := r.db.Table("notification_email").Find(&notifs).Offset(offset).Limit(limit).Error
	for i := range notifs {
		notifs[i].CreatedAt = util.FormatDate(notifs[i].CreatedAt)
		notifs[i].UpdatedAt = util.FormatDate(notifs[i].UpdatedAt)
	}

	return notifs, err
}

// count
func (r repository) Count(ctx context.Context) (int, error) {
	var item entity.NotificationEmail
	result := r.db.Table("notification_email").Find(&item)
	return int(result.RowsAffected), result.Error
}

// Create saves a new item record in the database.
func (r repository) Create(ctx context.Context, item entity.NotificationEmail) error {
	return r.db.Table("notification_email").Create(&item).Error
}

// Update ...
func (r repository) Update(ctx context.Context, id string, userNew entity.NotificationEmail) error {
	var item entity.NotificationEmail
	err := r.db.Table("notification_email").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}

	err = r.db.Table("notification_email").Where("id = ?", id).Updates(userNew).Error
	return err

}

// Delete ...
func (r repository) Delete(ctx context.Context, id string) error {
	var item entity.NotificationEmail
	err := r.db.Table("notification_email").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}
	err = r.db.Table("notification_email").Where("id = ?", id).Delete(item).Error
	return err
}
