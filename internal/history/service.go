package history

import (
	"context"
	"time"

	"gitlab.com/itbis/naura-api/internal/entity"
)

// Usecase ...
type Service interface {
	Get(ctx context.Context, id, partnerId string) (entity.History, error)
	Query(ctx context.Context, partnerId string, offset, limit int, startDate, endDate string, msisdn string) ([]entity.History, error)
	Count(ctx context.Context, partnerId string, startDate, endDate string, msisdn string) (int, error)
}

type service struct {
	repo           Repository
	contextTimeout time.Duration
}

// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
	}
}

// Count ...
func (s service) Count(ctx context.Context, partnerId string, startDate, endDate string, msisdn string) (int, error) {
	return s.repo.Count(ctx, partnerId, startDate, endDate, msisdn)
}

// Get ...
func (s service) Get(ctx context.Context, id, partnerId string) (entity.History, error) {
	item, err := s.repo.Get(ctx, id, partnerId)
	if err != nil {
		return item, err
	}

	return item, err
}

// Query ...
func (s service) Query(ctx context.Context, partnerId string, offset, limit int, startDate, endDate string, msisdn string) ([]entity.History, error) {
	items := make([]entity.History, 1)
	var err error
	items, err = s.repo.Query(ctx, partnerId, offset, limit, startDate, endDate, msisdn)
	if err != nil {
		return items, err
	}

	return items, err
}
