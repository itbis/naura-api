package history

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/itbis/naura-api/internal/audit_trail"
	"gitlab.com/itbis/naura-api/internal/user"
	"gitlab.com/itbis/naura-api/middleware"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/pagination"
	"gitlab.com/itbis/naura-api/shared"
)

// Handler  represent the httphandler
type handler struct {
	service      Service
	auditService audit_trail.Service
	userService  user.Service
	logger       logger.Logger
	middware     *middleware.GoMiddleware
}

// RegisterHandler ...
func RegisterHandler(e *echo.Echo, service Service, auditService audit_trail.Service, userService user.Service, logger logger.Logger, middware *middleware.GoMiddleware) {
	h := &handler{
		service:      service,
		auditService: auditService,
		userService:  userService,
		logger:       logger,
		middware:     middware,
	}
	v := e.Group("/naura/v1")
	v.GET("/history/:id", h.Get, h.middware.IsLoggedIn, h.middware.SetHeader)
	v.GET("/history", h.Query, h.middware.IsLoggedIn, h.middware.SetHeader)

}

type Request struct {
	PartnerId string `json:"partner_id"`
}


// Get will fetch the item based on given params
func (h *handler) Get(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		req.PartnerId = c.Request().Header.Get("partnerId")

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")
		partnerId := c.Request().Header.Get("partnerId")
		data, err := h.service.Get(ctx, id, partnerId)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "get/history", "success")

		resp = shared.Success(data)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Query(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		partnerId := c.Request().Header.Get("partnerId")
		startDate := c.QueryParam("start_date")
		endDate := c.QueryParam("end_date")
		msisdn := c.QueryParam("msisdn")

		if c.Request().Header.Get("rolesId") == "1" {
			partnerId = "all"
		}

		count, err := h.service.Count(ctx, partnerId, startDate, endDate, msisdn)
		if err != nil {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)
		}
		pages := pagination.NewFromRequest(c.Request(), count)
		data, err := h.service.Query(ctx, partnerId, pages.Offset(), pages.Limit(), startDate, endDate, msisdn)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		if len(data) < 1 {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)
		}

		h.auditService.Insert(ctx, c.Request().Header.Get("username"), "query/history", "success")

		pages.Items = data
		resp = shared.Success(pages)
		return c.JSON(resp.StatusCode(), resp)
	}
}
