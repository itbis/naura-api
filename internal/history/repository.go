package history

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access menus from the data source.
type Repository interface {
	Get(ctx context.Context, id, partnerId string) (entity.History, error)
	Count(ctx context.Context, partnerId string, startDate, endDate, msisdn string) (int, error)
	Query(ctx context.Context, partnerId string, offset, limit int, startDate, endDate, msisdn string) ([]entity.History, error)
}

// repository persists menus in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id, partnerId string) (entity.History, error) {

	var item entity.History
	var historyPartner []entity.HistoryPartner
	var historyRecycle []string
	var historySimswap []string

	filter := ""
	if partnerId != "all" {
		filter += " and partner_id='" + partnerId + "'"
	}

	var msisdn string
	sql := "SELECT msisdn, partner_id, is_active, date(created_at), date(created_at) FROM partner_account where msisdn=? " + filter + " order by date(created_at) desc"
	err := r.db.Raw(sql, id, partnerId).Scan(&item).Error

	msisdn = item.Msisdn
	if msisdn != "" {
		sql = "SELECT partner_id, transaction_id, action, created_at FROM partner_request where msisdn=? " + filter + " order by date(created_at) desc limit 3"
		err = r.db.Raw(sql, msisdn, partnerId).Scan(&historyPartner).Error

		sql = "SELECT date(created_at) FROM number_recycle_history where msisdn=? order by date(created_at) desc limit 3"
		err = r.db.Raw(sql, msisdn).Scan(&historyRecycle).Error

		sql = "SELECT date(created_at) FROM number_simswap_history where msisdn=? order by date(created_at) desc limit 3"
		err = r.db.Raw(sql, msisdn).Scan(&historySimswap).Error

	} else {
		return item, errors.New("record not found")
	}

	//TODO check nbp
	item.HistoryPartner = historyPartner
	item.HistorySimswap = historySimswap
	item.HistoryRecycle = historyRecycle

	return item, err
}

// Query
func (r repository) Query(ctx context.Context, partnerId string, offset, limit int, startDate, endDate, msisdn string) ([]entity.History, error) {
	//var itemsx []entity.History
	//itemsx := make([]entity.History, 0)
	itemsx := []entity.History{}
	itemsx_temp := []entity.History{}

	//item := entity.History{}

	sql := ""
	var err error
	filter := ""
	if msisdn != "" {
		filter += " and msisdn='" + msisdn + "'"
	}
	if startDate != "" && endDate != "" {
		filter += " and date(created_at) between '" + startDate + "' and '" + endDate + "'"
	}
	if partnerId != "all" {
		filter += " and partner_id='" + partnerId + "'"
	}

	sql = "SELECT * FROM partner_account where 1=1 " + filter + "  order by date(created_at) desc LIMIT ? OFFSET ?"
	err = r.db.Raw(sql, limit, offset).Scan(&itemsx_temp).Error
	
	for _, itemx := range itemsx_temp {

		//itemsx.PartnerId = itemx.PartnerId
		
		if itemx.Msisdn != "" {
			var sql string
			item := entity.History{}
			var historyPartner []entity.HistoryPartner
			var historyRecycle []string
			var historySimswap []string
			item.Msisdn = itemx.Msisdn
			item.CreatedAt = itemx.CreatedAt
			item.UpdatedAt = itemx.UpdatedAt
			item.IsActive = itemx.IsActive
			item.PartnerId=itemx.PartnerId

			sql = "SELECT partner_id, transaction_id, action, date(created_at) FROM partner_request where msisdn=?  and partner_id=? order by date(created_at) desc limit 3"
			err = r.db.Raw(sql, itemx.Msisdn, partnerId).Scan(&historyPartner).Error

			sql = "SELECT date(created_at) FROM number_recycle_history where msisdn=? order by date(created_at) desc limit 3"
			err = r.db.Raw(sql, itemx.Msisdn).Scan(&historyRecycle).Error

			sql = "SELECT date(created_at) FROM number_simswap_history where msisdn=? order by date(created_at) desc limit 3"
			err = r.db.Raw(sql, itemx.Msisdn).Scan(&historySimswap).Error

			//TODO check nbp
			item.HistoryPartner = historyPartner
			item.HistorySimswap = historySimswap
			item.HistoryRecycle = historyRecycle
			//append
			itemsx = append(itemsx, item)
		}

	}
	fmt.Println(itemsx)

	return itemsx, err
}

// count
func (r repository) Count(ctx context.Context, partnerId string, startDate, endDate, msisdn string) (int, error) {
	var count int
	sql := ""
	var err error
	filter := ""
	if msisdn != "" {
		filter += " and msisdn='" + msisdn + "'"
	}
	if startDate != "" && endDate != "" {
		filter += " and date(created_at) between '" + startDate + "' and '" + endDate + "'"
	}
	if partnerId != "all" {
		filter += " and partner_id='" + partnerId + "'"
	}

	sql = "SELECT count(id) as total FROM partner_account where 1=1 " + filter
	err = r.db.Raw(sql).Scan(&count).Error

	return count, err

}
