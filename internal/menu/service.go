package menu

import (
	"context"
	"time"

	"gitlab.com/itbis/naura-api/internal/entity"
)

// Usecase ...
type Service interface {
	Get(ctx context.Context, id string) (entity.Menu, error)
	GetByRoles(ctx context.Context, id string) ([]entity.MenuRoles, error)
	Query(ctx context.Context, offset, limit int) ([]entity.Menu, error)
	Count(ctx context.Context) (int, error)
	Create(ctx context.Context, menu entity.Menu) error
	Update(ctx context.Context, id string, menu entity.Menu) error
	Delete(ctx context.Context, id string) error
}

type service struct {
	repo           Repository
	contextTimeout time.Duration
}

// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
	}
}

// Count ...
func (s service) Count(ctx context.Context) (int, error) {
	return s.repo.Count(ctx)
}

// Get ...
func (s service) Get(ctx context.Context, id string) (entity.Menu, error) {
	item, err := s.repo.Get(ctx, id)
	if err != nil {
		return item, err
	}

	return item, err
}

// Get ...
func (s service) GetByRoles(ctx context.Context, id string) ([]entity.MenuRoles, error) {
	items, err := s.repo.GetByRoles(ctx, id)
	if err != nil {
		return items, err
	}
	result := []entity.MenuRoles{}
	for _, item := range items {
		result = append(result, item)
	}
	return result, nil
}

// Query ...
func (s service) Query(ctx context.Context, offset, limit int) ([]entity.Menu, error) {
	items, err := s.repo.Query(ctx, offset, limit)
	if err != nil {
		return items, err
	}

	return items, err
}

// Get ...
func (s service) Create(ctx context.Context, menu entity.Menu) error {
	err := s.repo.Create(ctx, menu)
	return err
}

func (s service) Update(ctx context.Context, id string, menu entity.Menu) error {
	err := s.repo.Update(ctx, id, menu)
	return err
}

func (s service) Delete(ctx context.Context, id string) error {
	err := s.repo.Delete(ctx, id)
	return err
}
