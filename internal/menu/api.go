package menu

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/internal/user"
	"gitlab.com/itbis/naura-api/middleware"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/pagination"
	"gitlab.com/itbis/naura-api/shared"
)

// Handler  represent the httphandler
type handler struct {
	service  Service
	userService user.Service
	logger   logger.Logger
	middware *middleware.GoMiddleware
}

// RegisterHandler ...
func RegisterHandler(e *echo.Echo, service Service, userService user.Service, logger logger.Logger, middware *middleware.GoMiddleware) {
	h := &handler{
		service:  service,
		userService: userService,
		logger:   logger,
		middware: middware,
	}

	v := e.Group("/naura/v1")
	v.GET("/menus/:id", h.Get, h.middware.IsLoggedIn)
	v.GET("/menus/roles/:id", h.GetByRoles, h.middware.IsLoggedIn)

	v.GET("/menus", h.Query, h.middware.IsLoggedIn)
	v.POST("/menus", h.Post, h.middware.IsLoggedIn)
	v.PUT("/menus/:id", h.Put, h.middware.IsLoggedIn)
	v.DELETE("/menus/:id", h.Delete, h.middware.IsLoggedIn)

}

type Request struct {
}

// Get will fetch the item based on given params
func (h *handler) Get(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")

		data, err := h.service.Get(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success(data)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) GetByRoles(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")

		data, err := h.service.GetByRoles(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success(data)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Query(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		count, err := h.service.Count(ctx)
		if err != nil {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)
		}
		pages := pagination.NewFromRequest(c.Request(), count)
		data, err := h.service.Query(ctx, pages.Offset(), pages.Limit())

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		pages.Items = data

		resp = shared.Success(pages)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Post(c echo.Context) error {

	var req entity.Menu
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		err = h.service.Create(ctx, req)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Put(c echo.Context) error {

	var req entity.Menu
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")
		err = h.service.Update(ctx, id, req)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Delete
func (h *handler) Delete(c echo.Context) error {

	var req entity.Menu
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.userService.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)
		}

		ctx := c.Request().Context()
		id := c.Param("id")
		err = h.service.Delete(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}

		resp = shared.Success("")
		return c.JSON(resp.StatusCode(), resp)
	}
}
