package menu

import (
	"context"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access menus from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.Menu, error)
	GetByRoles(ctx context.Context, id string) ([]entity.MenuRoles, error)
	Count(ctx context.Context) (int, error)
	Query(ctx context.Context, offset, limit int) ([]entity.Menu, error)
	Create(ctx context.Context, menu entity.Menu) error
	Update(ctx context.Context, id string, menu entity.Menu) error
	Delete(ctx context.Context, id string) error
}

// repository persists menus in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.Menu, error) {
	var menu entity.Menu
	err := r.db.Table("menus").First(&menu, "id=?", id).Error
	return menu, err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.Menu, error) {
	var menus []entity.Menu
	err := r.db.Table("menus").Find(&menus).Offset(offset).Limit(limit).Error
	return menus, err
}

// count
func (r repository) Count(ctx context.Context) (int, error) {
	var menu entity.Menu
	result := r.db.Table("menus").Find(&menu)
	return int(result.RowsAffected), result.Error
}

// Create saves a new menu record in the database.
func (r repository) Create(ctx context.Context, menu entity.Menu) error {
	return r.db.Table("menus").Create(&menu).Error
}

// Update ...
func (r repository) Update(ctx context.Context, id string, userNew entity.Menu) error {
	var menu entity.Menu
	err := r.db.Table("menus").First(&menu, "id=?", id).Error
	if err != nil {
		return err
	}

	err = r.db.Table("menus").Where("id = ?", id).Updates(userNew).Error
	return err

}

// Delete ...
func (r repository) Delete(ctx context.Context, id string) error {
	var menu entity.Menu
	err := r.db.Table("menus").First(&menu, "id=?", id).Error
	if err != nil {
		return err
	}
	err = r.db.Table("menus").Where("id = ?", id).Delete(menu).Error
	return err
}

// Query retrieves the menu records with the specified offset and limit from the database.
func (r repository) GetByRoles(ctx context.Context, id string) ([]entity.MenuRoles, error) {
	var menus []entity.MenuRoles
	/*err := r.db.With(ctx).
	Select().
	OrderBy("id").
	Offset(int64(offset)).
	Limit(int64(limit)).
	All(&menus)
	*/

	sql := "SELECT * FROM menus m LEFT join menus_roles mp on m.id=mp.menus_id where mp.roles_id=?"
	err := r.db.Raw(sql, id).Scan(&menus).Error
	return menus, err
}
