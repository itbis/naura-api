package audit_trail

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/itbis/naura-api/middleware"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/pagination"
	"gitlab.com/itbis/naura-api/shared"
)

// Handler  represent the httphandler
type handler struct {
	service  Service
	logger   logger.Logger
	middware *middleware.GoMiddleware
}

// RegisterHandler ...
func RegisterHandler(e *echo.Echo, service Service, logger logger.Logger, middware *middleware.GoMiddleware) {
	h := &handler{
		service:  service,
		logger:   logger,
		middware: middware,
	}

	v := e.Group("/naura/v1")
	v.GET("/audit-trail/:id", h.Get, h.middware.IsLoggedIn)
	v.GET("/audit-trail", h.Query, h.middware.IsLoggedIn)
	//v.POST("/audit-trail", h.Query, h.middware.IsLoggedIn)

}

type Request struct {
}



// Get will fetch the item based on given params
func (h *handler) Get(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.service.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		id := c.Param("id")

		/*if id == "" {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)
		}
		*/

		data, err := h.service.Get(ctx, id)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		resp = shared.Success(data)
		return c.JSON(resp.StatusCode(), resp)
	}
}

// Get will fetch the item based on given params
func (h *handler) Query(c echo.Context) error {

	var req Request
	var resp shared.Response
	getInfo := h.middware.GetInfo(c)
	getCurrentUser, _ := h.service.CheckEmailUser(getInfo.Email)
	fmt.Println(getCurrentUser.Blacklist)
	if getCurrentUser.Blacklist == "disallowed" {
		resp = shared.StatusForbidden("")
		return c.JSON(resp.StatusCode(), resp)
	}else {

		err := c.Bind(&req)
		if err != nil {
			resp = shared.StatusUnprocessableEntity("")
			return c.JSON(resp.StatusCode(), resp)
		}

		var ok bool
		if ok, err = shared.IsRequestValid(&req); !ok {
			resp = shared.StatusBadRequest("")
			return c.JSON(resp.StatusCode(), resp)

		}

		ctx := c.Request().Context()
		username := c.QueryParam("username")
		/*if username == "" {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)
		}
		*/

		count, err := h.service.Count(ctx, username)
		if err != nil {
			resp = shared.StatusNotFound("")
			return c.JSON(resp.StatusCode(), resp)
		}
		pages := pagination.NewFromRequest(c.Request(), count)
		data, err := h.service.Query(ctx, pages.Offset(), pages.Limit(), username)

		if err != nil {
			resp = shared.StatusInternalServerError(err.Error())
			return c.JSON(resp.StatusCode(), resp)
		}
		pages.Items = data

		resp = shared.Success(pages)
		return c.JSON(resp.StatusCode(), resp)
	}
}
