package audit_trail

import (
	"context"

	"gitlab.com/itbis/naura-api/internal/entity"
	"gitlab.com/itbis/naura-api/pkg/logger"
	"gitlab.com/itbis/naura-api/pkg/util"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access menus from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.AuditTrail, error)
	Count(ctx context.Context, id string) (int, error)
	Query(ctx context.Context, offset, limit int, id string) ([]entity.AuditTrail, error)
	Insert(ctx context.Context, username, action, desc string) error
	CheckEmailUser(email string) (entity.User, int)
}

// repository persists menus in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.AuditTrail, error) {
	var item entity.AuditTrail
	err := r.db.Table("audit_trail").First(&item, "username=?", id).Error
	item.CreatedAt = util.FormatDate(item.CreatedAt)
	return item, err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int, id string) ([]entity.AuditTrail, error) {
	var item []entity.AuditTrail
	sql := "SELECT * FROM audit_trail where username like '%" + id + "%'"
	err := r.db.Raw(sql).Scan(&item).Error
	//err := r.db.Table("audit_trail").Find(&item).Offset(offset).Limit(limit).Error
	for i := range item {
		item[i].CreatedAt = util.FormatDate(item[i].CreatedAt)

	}

	return item, err
}

// count
func (r repository) Count(ctx context.Context, id string) (int, error) {
	var count int
	sql := "SELECT count(id) as total FROM audit_trail where username like '%" + id + "%'"
	err := r.db.Raw(sql).Scan(&count).Error

	return count, err

}

// count
func (r repository) Insert(ctx context.Context, username, action, desc string) error {
	var auditTrail entity.AuditTrail
	/*auditTrail := entity.AuditTrail{Username: username, Action: action, Desc: desc}
	result := r.db.Create(&auditTrail)
	return result.Error*/
	err := r.db.Raw("INSERT INTO audit_trail (`username`,`action`,`desc`) values (?,?,?)", username, action, desc).Scan(&auditTrail).Error
	return err
}

func (r repository) CheckEmailUser(email string) (entity.User, int) {
	var user entity.User
	result := r.db.Table("users").First(&user, "email=?", email)
	return user, int(result.RowsAffected)
}