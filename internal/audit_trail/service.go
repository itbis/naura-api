package audit_trail

import (
	"context"
	"time"

	"gitlab.com/itbis/naura-api/internal/entity"
)

// Usecase ...
type Service interface {
	Get(ctx context.Context, id string) (entity.AuditTrail, error)
	Query(ctx context.Context, offset, limit int, id string) ([]entity.AuditTrail, error)
	Count(ctx context.Context, id string) (int, error)
	Insert(ctx context.Context, username, action, desc string) error
	CheckEmailUser(email string) (entity.User, int)
}

type service struct {
	repo           Repository
	contextTimeout time.Duration
}



// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
	}
}

// Get ...
func (s service) Get(ctx context.Context, id string) (entity.AuditTrail, error) {
	item, err := s.repo.Get(ctx, id)
	if err != nil {
		return item, err
	}

	return item, err
}

// Count ...
func (s service) Count(ctx context.Context, id string) (int, error) {
	return s.repo.Count(ctx, id)
}

// Query ...
func (s service) Query(ctx context.Context, offset, limit int, id string) ([]entity.AuditTrail, error) {
	items, err := s.repo.Query(ctx, offset, limit, id)
	if err != nil {
		return items, err
	}

	return items, err
}

// Query ...
func (s service) Insert(ctx context.Context, username, action, desc string) error {
	return s.repo.Insert(ctx, username, action, desc)
}

//Check EmailUser..
func (s service) CheckEmailUser(email string) (entity.User, int) {
	user, i := s.repo.CheckEmailUser(email)
	return user, i
}





