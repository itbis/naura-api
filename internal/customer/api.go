package customer

import (
	"net/http"

	routing "github.com/go-ozzo/ozzo-routing/v2"
	"gitlab.com/itbis/naura-api/internal/errors"
	"gitlab.com/itbis/naura-api/pkg/log"
	"gitlab.com/itbis/naura-api/pkg/pagination"
)

// RegisterHandlers sets up the routing of the HTTP handlers.
func RegisterHandlers(r *routing.RouteGroup, service Service, authHandler routing.Handler, logger log.Logger) {
	res := resource{service, logger}

	// the following endpoints require a valid JWT
	r.Use(authHandler)

	r.Get("/customers/<id>", res.get)
	r.Get("/customers", res.query)

	//r.Post("/customers", res.create)
	//r.Put("/customers/<id>", res.update)
	//r.Delete("/customers/<id>", res.delete)
}

type resource struct {
	service Service
	logger  log.Logger
}

func (r resource) get(c *routing.Context) error {
	customer, err := r.service.Get(c.Request.Context(), c.Param("id"))
	if err != nil {
		return err
	}

	return errors.Success(customer)
}

func (r resource) query(c *routing.Context) error {
	ctx := c.Request.Context()
	count, err := r.service.Count(ctx)
	if err != nil {
		return err
	}
	pages := pagination.NewFromRequest(c.Request, count)
	customers, err := r.service.Query(ctx, pages.Offset(), pages.Limit())
	if err != nil {
		return err
	}
	pages.Items = customers
	return errors.Success(pages)
}

func (r resource) create(c *routing.Context) error {
	var input CreateRequest
	if err := c.Read(&input); err != nil {
		r.logger.With(c.Request.Context()).Info(err)
		return errors.BadRequest("")
	}
	customer, err := r.service.Create(c.Request.Context(), input)
	if err != nil {
		return err
	}

	return errors.SuccessWithStatus(customer, http.StatusCreated)
}

func (r resource) update(c *routing.Context) error {
	var input UpdateRequest
	if err := c.Read(&input); err != nil {
		r.logger.With(c.Request.Context()).Info(err)
		return errors.BadRequest("")
	}

	customer, err := r.service.Update(c.Request.Context(), c.Param("id"), input)
	if err != nil {
		return err
	}

	return errors.Success(customer)

}

func (r resource) delete(c *routing.Context) error {
	customer, err := r.service.Delete(c.Request.Context(), c.Param("id"))
	if err != nil {
		return err
	}

	return errors.Success(customer)
}
